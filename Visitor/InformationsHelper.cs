﻿namespace Visitor
{
    /// <summary>
    /// Main Informations.
    /// </summary>
    internal class InformationsHelper
    {
        public const string StartScan = "Start Scaning: ";
        public const string StopScan = "Stop Scaning: ";
        public const string FileFound = "\tFile Found: ";
        public const string DirectoryFound = "Directory Found: ";
        public const string FilterFileFound = "\tFilter File Found: ";
        public const string FilterDirectoryFound = "Filter Directory Found: ";
        public const string PleaseInsertPath = "Please insert path! (On example: D:\\Games)";
    }
}
