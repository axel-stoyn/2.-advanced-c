﻿namespace Visitor
{
    /// <summary>
    /// Current action.
    /// </summary>
    internal class CurrentAction
    {
        /// <summary>
        /// Get or set action.
        /// </summary>
        public UserAction Action { get; set; }

        /// <summary>
        /// Continue search Files/Folders.
        /// </summary>
        public static CurrentAction ContinueSearch => new CurrentAction { Action = UserAction.Continue };
    }
}
