﻿using System;

namespace Visitor
{
    /// <summary>
    /// Informations.
    /// </summary>
    internal class Informations
    {
        /// <summary>
        /// Get Informations.
        /// </summary>
        /// <returns>Informations.</returns>
        public static void GetInformations(FileSystemVisitor fsv)
        {
            fsv.Start += (s, e) =>
            {
                Console.WriteLine(InformationsHelper.StartScan);
            };

            fsv.Finish += (s, e) =>
            {
                Console.WriteLine(InformationsHelper.StopScan);
                Console.ReadKey();
            };

            fsv.FileFound += (s, e) =>
            {
                Console.WriteLine(InformationsHelper.FileFound + e.FoundItem.Name);
            };

            fsv.DirectoryFound += (s, e) =>
            {
                Console.WriteLine(InformationsHelper.DirectoryFound + e.FoundItem.Name);
                if (e.FoundItem.Name.Length == 4)
                {
                    e.UserAction = UserAction.Stop;
                }
            };

            fsv.FilterFileFound += (s, e) =>
            {
                Console.WriteLine(InformationsHelper.FilterFileFound + e.FoundItem.Name);
            };

            fsv.FilterDirectoryFound += (s, e) =>
            {
                Console.WriteLine(InformationsHelper.FilterDirectoryFound + e.FoundItem.Name);
                if (e.FoundItem.Name.Length == 4)
                {
                    e.UserAction = UserAction.Stop;
                }
            };

            foreach (var fileSysInfo in fsv.GetFileInfo())
            {
                Console.WriteLine(fileSysInfo);
            }
        }
    }
}
