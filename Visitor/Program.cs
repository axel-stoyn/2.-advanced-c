﻿using System;

namespace Visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(InformationsHelper.PleaseInsertPath);
            string path = Console.ReadLine(); // On Example "D:\\Games";
            FileSystemVisitor fsv = new FileSystemVisitor(path, new ItemProcessAction(), (info) => true);
            Informations.GetInformations(fsv);
        }
    }
}

