﻿namespace Visitor
{
    /// <summary>
    /// Enumerable actions.
    /// </summary>
    public enum UserAction
    {
        Continue = 1,
        Skip = 2,
        Stop = 3
    }
}
