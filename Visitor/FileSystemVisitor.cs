﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Visitor
{
    /// <summary>
    /// By Pass Tree Folders.
    /// </summary>
    public class FileSystemVisitor
    {
        #region Fields

        private DirectoryInfo directoryInfo;
        private readonly Func<FileSystemInfo, bool> filter;
        private readonly IItemProcess _iitemprocess;

        #endregion

        #region Events

        internal event EventHandler<EventArgs> Start;
        internal event EventHandler<EventArgs> Finish;
        internal event EventHandler<ItemFindEvent<FileSystemInfo>> FileFound;
        internal event EventHandler<ItemFindEvent<FileSystemInfo>> FilterFileFound;
        internal event EventHandler<ItemFindEvent<DirectoryInfo>> DirectoryFound;
        internal event EventHandler<ItemFindEvent<DirectoryInfo>> FilterDirectoryFound;

        #endregion

        /// <summary>
        /// Constructor ByPass.
        /// </summary>
        /// <param name="path"> Folder way.</param>
        /// <param name="itemProcess"> Action item process.</param>
        /// <param name="filter"> Yes or no information/folder/file.</param>
        public FileSystemVisitor(string path, IItemProcess itemProcess, Func<FileSystemInfo, bool> filter = null)
            : this(new DirectoryInfo(path), itemProcess, filter)
        {
        }

        /// <summary>
        /// Constructor ByPass.
        /// </summary>
        /// <param name="path"> Folder way.</param>
        /// <param name="itemProcess"> Action item process.</param>
        /// <param name="filter"> Yes or no information/folder/file.</param>
        public FileSystemVisitor(DirectoryInfo directoryInfo, IItemProcess itemProcess, Func<FileSystemInfo, bool> filter)
        {
            this.directoryInfo = directoryInfo;
            this._iitemprocess = itemProcess;
            this.filter = filter;
        }

        /// <summary>
        /// Get file info.
        /// </summary>
        /// <returns>Events</returns>returns>
        public IEnumerable<FileSystemInfo> GetFileInfo()
        {
            GetEvent(Start, new EventArgs());
            foreach (var fileSystemInfo in BypassFile(directoryInfo, CurrentAction.ContinueSearch))
            {
                yield return fileSystemInfo;
            }
            GetEvent(Finish, new EventArgs());
        }

        /// <summary>
        /// By pass File/Folder.
        /// </summary>
        /// <param name="directory"> Current directory.</param>
        /// <param name="currentAction"> Current Action.</param>
        /// <returns>Information about file derictory</returns>
        private IEnumerable<FileSystemInfo> BypassFile(DirectoryInfo directory, CurrentAction currentAction)
        {
            foreach (var fileSystemInfo in directory.EnumerateFileSystemInfos())
            {
                if (fileSystemInfo is FileInfo file)
                {
                    currentAction.Action = ProcessFile(file);
                }

                if (fileSystemInfo is DirectoryInfo dir)
                {
                    currentAction.Action = ProcessDirectory(dir);
                    if (currentAction.Action == UserAction.Continue)
                    {
                        yield return dir;
                        foreach (var innerInfo in BypassFile(dir, currentAction))
                        {
                            yield return innerInfo;
                        }
                        continue;
                    }
                }

                if (currentAction.Action == UserAction.Stop)
                {
                    yield break;
                }

                yield return fileSystemInfo;
            }
        }

        /// <summary>
        /// File process.
        /// </summary>
        /// <param name="file"> Current file.</param>
        /// <returns>Information about file derictory</returns>
        private UserAction ProcessFile(FileInfo file)
        {
            return _iitemprocess.ItemProcess(file, filter, FileFound, FilterFileFound, GetEvent);
        }

        /// <summary>
        /// Folder process.
        /// </summary>
        /// <param name="directory"> Current directory.</param>
        /// <returns>Information about folder derictory</returns>
        private UserAction ProcessDirectory(DirectoryInfo directory)
        {
            return _iitemprocess.ItemProcess(directory, filter, DirectoryFound, FilterDirectoryFound, GetEvent);
        }

        /// <summary>
        /// Events.
        /// </summary>
        public void GetEvent<T>(EventHandler<T> someEvent, T args)
        {
            someEvent?.Invoke(this, args);
        }
    }
}
