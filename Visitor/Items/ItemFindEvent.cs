﻿using System;

namespace Visitor
{
    /// <summary>
    /// Item Find Event.
    /// </summary>
    public class ItemFindEvent<FileSystemInfo> : EventArgs
    {
        /// <summary>
        /// Get or set found item.
        /// </summary>
        public FileSystemInfo FoundItem { get; set; }

        /// <summary>
        /// Get or set user action.
        /// </summary>
        public UserAction UserAction { get; set; }
    }
}
