﻿using System;
using System.IO;

namespace Visitor
{
    /// <summary>
    /// Item Process.
    /// </summary>
    public class ItemProcessAction : IItemProcess
    {
        /// <summary>
        /// Main process with files or folders.
        /// </summary>
        /// <returns>Events where put file or folders.</returns>
        public UserAction ItemProcess<T>(T file, Func<FileSystemInfo, bool> filter, EventHandler<ItemFindEvent<T>> directoryFound, EventHandler<ItemFindEvent<T>> filterDirectoryFound, 
            Action<EventHandler<ItemFindEvent<T>>, ItemFindEvent<T>> eventEmmit) where T : FileSystemInfo
        {
            ItemFindEvent<T> args = new ItemFindEvent<T>
            {
                FoundItem = file,
                UserAction = UserAction.Continue
            };
            eventEmmit(directoryFound, args);

            if (filter(file))
            {
                args = new ItemFindEvent<T>
                {
                    FoundItem = file,
                    UserAction = UserAction.Continue
                };
                eventEmmit(filterDirectoryFound, args);
                return args.UserAction;
            }

            return UserAction.Skip;
        }
    }
}
