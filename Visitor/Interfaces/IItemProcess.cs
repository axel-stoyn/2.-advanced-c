﻿using System;
using System.IO;

namespace Visitor
{        
    /// <summary>
    /// see class Item Process.
    /// </summary>
    public interface IItemProcess
    {
        UserAction ItemProcess<T>(T file, Func<FileSystemInfo, bool> filter, EventHandler<ItemFindEvent<T>> itemFound, EventHandler<ItemFindEvent<T>> filterItemFound,
            Action<EventHandler<ItemFindEvent<T>>, ItemFindEvent<T>> eventEmmit) 
            where T : FileSystemInfo;
    }
}
