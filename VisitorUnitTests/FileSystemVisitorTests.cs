﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Visitor;
using System.IO;
using Moq;

namespace VisitorUnitTests
{
    [TestClass]
    public class FileSystemVisitorTests
    {

        #region Moq

        private IItemProcess _itemProcess;
        private FileSystemInfo fsv;
        private Mock<FileSystemInfo> _fileSystemInfo;

        #endregion

        [TestInitialize]
        public void TestInitialize()
        {
            _itemProcess = new ItemProcessAction();
            _fileSystemInfo = new Mock<FileSystemInfo>();
        }

        [TestMethod]
        public void ItemFound()
        {
            fsv = _fileSystemInfo.Object;
            int count = 0;

            _itemProcess.ItemProcess(fsv, null, (s, e) => count++, null, GetEvent);

            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public void FilterItemFound()
        {
            fsv = _fileSystemInfo.Object;
            int count = 0;

            _itemProcess.ItemProcess(fsv, info => true, (s, e) => count++, (s, e) => count++, GetEvent);

            Assert.AreEqual(2, count);
        }

        [TestMethod]
        public void ItemFoundThenContin()
        {
            fsv = _fileSystemInfo.Object;

            var action = _itemProcess.ItemProcess(fsv, null, (s, e) => { }, null, GetEvent);

            Assert.AreEqual(UserAction.Continue, action);
        }

        [TestMethod]
        public void FilteItemFoundThenContin()
        {
            fsv = _fileSystemInfo.Object;

            var action = _itemProcess.ItemProcess(fsv, info => true, (s, e) => { }, (s, e) => { }, GetEvent);

            Assert.AreEqual(UserAction.Continue, action);
        }

        [TestMethod]
        public void FindItemButSkip()
        {
            fsv = _fileSystemInfo.Object;
            int count = 0;

            var action = _itemProcess.ItemProcess(fsv, info => true, (s, e) =>
                {
                    count++;
                    e.UserAction = UserAction.Skip;
                }, (s, e) => count++, GetEvent);

            Assert.AreEqual(UserAction.Skip, action);
            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public void FilterFoundTehnSkip()
        {
            fsv = _fileSystemInfo.Object;
            int count = 0;

            var action = _itemProcess.ItemProcess(fsv, info => true, (s, e) => count++, (s, e) =>
                {
                    count++;
                    e.UserAction = UserAction.Skip;
                }, GetEvent);

            Assert.AreEqual(UserAction.Skip, action);
            Assert.AreEqual(2, count);
        }

        [TestMethod]
        public void FoundItemThenStop()
        {
            fsv = _fileSystemInfo.Object;
            int count = 0;

            var action = _itemProcess.ItemProcess(fsv, info => true, (s, e) =>
                {
                    count++;
                    e.UserAction = UserAction.Stop;
                }, (s, e) => count++, GetEvent);

            Assert.AreEqual(UserAction.Stop, action);
            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public void FilterFoundItemThenStop()
        {
            fsv = _fileSystemInfo.Object;
            int count = 0;

            var action = _itemProcess.ItemProcess(fsv, info => true, (s, e) => count++, (s, e) =>
                {
                    count++;
                    e.UserAction = UserAction.Stop;
                }, GetEvent);

            Assert.AreEqual(UserAction.Stop, action);
            Assert.AreEqual(2, count);
        }

        private void GetEvent<T>(EventHandler<T> someEvent, T args)
        {
            someEvent?.Invoke(this, args);
        }
    }
}
